package com.example.lucina.models

import android.content.Context
import com.example.lucina.Classes.Request
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.lang.Exception

class APIModel(request: Request, context: Context) {
    val r: Request = request
    var res: String = ""

    fun launchRequest(blocking: Boolean = false): String {
        if (blocking) {
            runBlocking {
                try {
                    res = r.runRequest()
                } catch (e: Exception) {
                    System.out.println(e.message)
                }
            }
        }else{
            GlobalScope.launch {
                try {
                    res = r.runRequest()
                } catch (e: Exception) {
                    System.out.println(e.message)
                }
            }
        }

        return res
    }
}
