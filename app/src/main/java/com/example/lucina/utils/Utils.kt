package com.example.lucina.utils

fun toJsonString(keys: Array<String>, values: Array<String>): String{
    var res = "{"
    for (i in 0..keys.size-1){
        if (i > 0)
            res += ","
        res += "\""+keys[i]+"\": \""
        res += values[i]+"\""
    }
    res += "}"
    return res
}