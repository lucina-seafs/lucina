package com.example.lucina.activities

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.lucina.Classes.Request
import com.example.lucina.Classes.User
import com.example.lucina.Classes.UserLocation
import com.example.lucina.R
import com.example.lucina.models.APIModel
import com.example.lucina.utils.toJsonString


class MainActivity : AppCompatActivity(), LocationListener {
    lateinit var txt_county: TextView
    lateinit var txt_username: TextView
    lateinit var txt_nearCount: TextView
    lateinit var bt_refresh: Button

    lateinit var user: User
    lateinit var userLoc: UserLocation

    var refresh_countdown = 30

    var need_refresh = true // to refresh manually location name


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        user = intent.getParcelableExtra<User>("user") as User

        txt_county = findViewById<TextView>(R.id.txt_county)
        txt_username = findViewById<TextView>(R.id.txt_username)
        txt_nearCount = findViewById<TextView>(R.id.txt_nearCount)
        bt_refresh = findViewById<Button>(R.id.bt_refresh)

        txt_county.text = getString(R.string.location_loading)
        txt_nearCount.text =getString(R.string.loading)
        txt_username.text = user.id

        // LF location in background
        getLocation()

        bt_refresh.setOnClickListener() {
            System.out.println("refresh")
            need_refresh = true
            txt_county.text = getString(R.string.location_loading)
            txt_nearCount.text =getString(R.string.loading)
            bt_refresh.isEnabled = false
            bt_refresh.isClickable = false
            countdown()
            getLocation()
        }
    }

    override fun onResume() {
        val delay: Long = 10000
        need_refresh = true

        val r = Runnable {
            //tasks
            if (::userLoc.isInitialized && ::user.isInitialized) {
                userLoc.refreshNearUsersCount(user)
                txt_nearCount.text = userLoc.nearUsersCount.toString() + " users nearby"
            }
        }
        Handler(Looper.getMainLooper()).postDelayed(r, delay)

        super.onResume()
    }

    private fun countdown(){
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                bt_refresh.text = (millisUntilFinished / 1000).toString()
            }
            override fun onFinish() {
                // do something after countdown is done ie. enable button, change color
                bt_refresh.text = "Refresh"
                bt_refresh.isEnabled = true
                bt_refresh.isClickable = true
            }
        }.start()
    }

    fun getLocation(){
        System.out.println("update location")
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val gpsEnabled: Boolean = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            Toast.makeText(this, "Veuillez activer le GPS", Toast.LENGTH_LONG)
            System.out.println("Pas de GPS")
        } else {
            val api_url = getString(R.string.api_url)

            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                val PERMISSIONS = arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                )
                ActivityCompat.requestPermissions(this, PERMISSIONS, 112)
            }

            val criteria = Criteria()

            try {
                //val provider = locationManager.getBestProvider(criteria, true) as String
                locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
                try{
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this)
                }catch (e: Exception){
                    val criteria = Criteria()
                    val provider: String = locationManager.getBestProvider(criteria, true)!!
                    System.out.println(provider)
                    locationManager.requestLocationUpdates(provider, 0, 0f, this)
                }


            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }
    }

    override fun onLocationChanged(loc: Location) {
        System.out.println("Location has changed !")

        if (need_refresh){
            val location = UserLocation("", 0)
            location.setLocation(loc, this)
            if (::user.isInitialized) {
                // TODO: Send location to API server
                val api_url = getString(R.string.api_url)
                val parameters = toJsonString(arrayOf("county"), arrayOf(location.county!!))
                val request = Request(
                        "http://$api_url",
                        "POST",
                        "/county",
                        10302,
                        user.token,
                        parameters
                )
                System.out.println("refresh = " + request.toString())
                val res = APIModel(request, this).launchRequest(true)

                location.refreshNearUsersCount(user)

                txt_county.text = location.county
                txt_nearCount.text = location.nearUsersCount.toString() + " users nearby"
                userLoc = location
                need_refresh = (location.county == "")
            }
        }

    }
    override fun onStatusChanged(var1: String?, var2: Int, var3: Bundle?){
        System.out.println(var1 + " : " + var2 + " : " + var3)
    }

}
