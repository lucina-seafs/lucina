package com.example.lucina.activities

import android.content.Intent


import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lucina.R
import com.example.lucina.Classes.UserLocation
import com.example.lucina.Classes.Request
import com.example.lucina.Classes.User
import com.example.lucina.models.*
import com.example.lucina.utils.toJsonString


class LoginActivity : AppCompatActivity() {

    private lateinit var bt_login: Button
    private lateinit var et_id: EditText
    private lateinit var et_password: EditText

    private lateinit  var loc: UserLocation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //manage login

        // init items
        et_id = findViewById(R.id.et_id)
        et_password = findViewById(R.id.et_password)
        bt_login = findViewById(R.id.bt_login)

        bt_login.setOnClickListener() {
            val keys = arrayOf("id", "pass")
            val id = et_id.getText().toString()
            val pass = et_password.getText().toString()
            val values = arrayOf(id, pass)
            val parameters = toJsonString(keys, values)
            //API login
            val request = Request(
                "http://"+getString(R.string.api_url),
                "POST",
                "/login",
                10302,
                null,
                parameters
            )
            try {
                // verify credentials
                val result = APIModel(request, this).launchRequest(true)

                System.out.println("res = " + result)
                //TODO: change condition and server to validate only on SECCESSFUL LOGIN response
                if (result != "FAILED") {
                    val user = this.login(id, result)

                    // Open Main Activity
                    val intent = Intent(this, MainActivity::class.java).apply {
                        putExtra("user", user)
                    }
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Connexion failed", Toast.LENGTH_LONG)
                }

            } catch (e: Exception) {
                System.out.println(e)
            }
        }
    }


    fun login(id: String, token: String): User {

        // Prep test user
        val user = User(id, token)
        return user

    }


}
