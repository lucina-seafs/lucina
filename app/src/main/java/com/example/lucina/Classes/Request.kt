package com.example.lucina.Classes

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*


class Request(val uri: String,
              val requestMethod: String,
              val urlPath: String = "/",
              val listenPort: Int,
              val authToken: String? = null,
              val parameters: String? = null) {

    suspend fun runRequest(): String {
        val client = HttpClient()
        val urlString = uri+":"+listenPort+urlPath
        System.out.println(this.toString())
        val httpResponse = client.request<String>{
            url(urlString)
            method = HttpMethod.parse(requestMethod)

            if (authToken != null)
                header("Token", authToken)
            if (requestMethod == "POST" && parameters != null) {
                body = parameters
            }
        }
        System.out.println("response = " + httpResponse)
        return httpResponse
    }

    override fun toString(): String{
        var command = "curl "
        if (authToken != null){
            command += "-H \"Token: $authToken\" "
        }
        if (parameters != null && parameters != ""){
            command += "-d \"$parameters\" "
        }
        val parsedUrlPath= urlPath.replace(" ", "%20")

        command += uri+":"+listenPort+parsedUrlPath
        return command
    }
}