package com.example.lucina.Classes

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Parcel
import android.os.Parcelable
import androidx.core.app.ActivityCompat
import com.example.lucina.R
import com.example.lucina.models.APIModel
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.*
import org.json.JSONObject



data class UserLocation(var county: String?,
                        var nearUsersCount: Int): Parcelable, LocationListener{

    lateinit var context: Context
    private val REQUEST = 112
    lateinit var location: Location

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    fun setLocation(l: Location, ctx: Context) {
        this.location = l
        this.context = ctx
        System.out.println("setLocation")
        this.launch()
    }

    override fun toString(): String{
        System.out.println("county : " + county)
        if (county != null) {
            return county.toString()
        }else{
            return "no county"
        }
    }

    override fun onLocationChanged(loc: Location) {
        location = loc
    }


    fun toParameter(): String{
        return "$county"
    }


    fun launch(): String {
        val location: Location? = getLastBestLocation()
        System.out.println("location = $location")
        var county: String = ""
        try {
            val lonlat: String = "[longitude: ${location!!.longitude}, latitude: ${location.latitude}]"
            // Search for location
            runBlocking {
                county = getCountyName(location.longitude, location!!.latitude)
                changeCounty(county)
            }
        } catch (e: Exception) {
            System.out.println("unknown error" + e)
        }

        return county
    }

    private fun changeCounty(count: String){
        this.county = count
        System.out.println("county was found: "+this.county)
    }

    private fun getLastBestLocation(): Location? {
        System.out.println("currentcounty : $county")
        if (ActivityCompat.checkSelfPermission(
                this.context, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this.context, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            val PERMISSIONS = arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            ActivityCompat.requestPermissions((this.context as Activity?)!!, PERMISSIONS, REQUEST)
            System.out.println("pas de permission")
        }else {
            val mLocationManager:  LocationManager =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            var locationNet: Location? =
                mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            var GPSLocationTime: Long = 0

            var NetLocationTime: Long = 0
            if (null != locationNet) {
                NetLocationTime = locationNet.getTime()
            }else{
                // If we don't have a last known location from other app :
                locationNet = location
            }
            return locationNet
        }
        return null
    }

    suspend fun getCountyName(longitude: Double, latitude: Double): String{
        val httpClient: HttpClient = HttpClient()
        val url = "https://us1.locationiq.com/v1/reverse.php?key="+
                context.resources.getString(R.string.reverse_geocode_key)+
                "&lat=" + latitude.toString() +
                "2&lon=" + longitude.toString() +
                "&format=json"
        val htmlContent = httpClient.request<String> {
            url(url)
            method = HttpMethod.Get
        }
        return JSONObject(htmlContent).getJSONObject("address").getString("county")
    }

    fun refreshNearUsersCount(user: User){
        val api_url = context.getString(R.string.api_url)
        // Get near users count
        val request = Request("http://$api_url", "GET",
            "/county/$county", 10302, user.token)
        val res = APIModel(request, context).launchRequest(true)
        System.out.println("nearUsers: $res")
        if (res != ""){
            nearUsersCount = res.toInt()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(county)
        parcel.writeInt(nearUsersCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserLocation> {
        override fun createFromParcel(parcel: Parcel): UserLocation {
            return UserLocation(parcel)
        }

        override fun newArray(size: Int): Array<UserLocation?> {
            return arrayOfNulls(size)
        }
    }
}