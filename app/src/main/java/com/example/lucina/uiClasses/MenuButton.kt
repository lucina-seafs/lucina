package com.example.lucina.uiClasses

import android.content.Context
import android.graphics.Color
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import com.example.lucina.R

//globals
val BT_REDUCE = 1
val BT_EXTEND = 2

class MenuButton(context: Context, attr: AttributeSet?): AppCompatButton(context, attr) {
    val DP_SCALE = resources.displayMetrics.density

    var bt_name = ""
    var txt = ""

    init {
        listOfButtons += this
        textSize = 20.0F
        text = txt
        background = resources.getDrawable(R.drawable.menu_bt_background_shape)
        //setBackgroundColor(1)

        setOnLongClickListener{
            setBackgroundColor(resources.getColor(R.color.purple_700))
            setState(BT_EXTEND)
            return@setOnLongClickListener true
        }
        setOnClickListener{
            setBackgroundColor(resources.getColor(R.color.teal_200))
        }
        setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_UP -> {
                    if (isOneButtonActive) {1
                        object : CountDownTimer(3000, 3000) {
                            override fun onTick(p0: Long) {
                                null
                            }
                            override fun onFinish() {
                                setState(BT_REDUCE)
                                isOneButtonActive = false
                            }
                        }.start()
                    }
                }
                else -> return@setOnTouchListener false
            }
            return@setOnTouchListener false
        }
    }

    private fun setState(state: Int) {
        when (state){
            BT_REDUCE -> {
                setLayoutParams (LinearLayout.LayoutParams((80*DP_SCALE).toInt(), (80*DP_SCALE).toInt()))
                text = bt_name
            }
            BT_EXTEND -> {
                resetAllButtons()
                setLayoutParams (LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, (80*DP_SCALE).toInt()))
                isOneButtonActive = true
            }
        }
    }

    fun setContent(color: Color, text: String, id: String){
        setBackgroundColor(resources.getColor(R.color.teal_200))
    }

    companion object{
        var isOneButtonActive = false
        var listOfButtons: Array<MenuButton> = arrayOf()

        fun resetAllButtons(){
            for (bt in listOfButtons){
                bt.setState(BT_REDUCE)
            }
        }
    }
}

